package ru.axbit.stevl.booksauthorsrest.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import ru.axbit.stevl.booksauthorsrest.entity.Author;

/**
 *
 * @author SteVL
 */
@Service
public class AuthorService {

    Map<Long, Author> database = new HashMap<>();

    public Author findById(Long id) {
        return database.get(id);
    }

    public List<Author> getAll() {
        return new ArrayList<>(database.values());
    }

    public void create(Author author) {
        database.put(author.getId(), author);
    }

    public void update(Author updatableAuthor) {
        Author thisAuthor=database.get(updatableAuthor.getId());
        thisAuthor.setFirstName(updatableAuthor.getFirstName());
        thisAuthor.setMiddleName(updatableAuthor.getMiddleName());
        thisAuthor.setLastName(updatableAuthor.getLastName());  
    }
    
    public void delete (Long id){
        database.remove(id);
    }
}
