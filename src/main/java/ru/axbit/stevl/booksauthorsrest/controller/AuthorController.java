package ru.axbit.stevl.booksauthorsrest.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.axbit.stevl.booksauthorsrest.entity.Author;
import ru.axbit.stevl.booksauthorsrest.service.AuthorService;

/**
 *
 * @author SteVL
 */
@RestController
@RequestMapping("/authors")
public class AuthorController {

    @Autowired
    AuthorService service;// это DAO?
    //Для теста, понимания, просто вернём строку.

    @GetMapping("/test")
    public ResponseEntity<String> getFirstName() {
        return new ResponseEntity<String>("Test author name", HttpStatus.OK);
        //В итоге возвращает просто строку, но не JSON-формат
    }

    @GetMapping("/{id}")
    public ResponseEntity<Author> getAuthorById(@PathVariable Long id) {
        return new ResponseEntity<Author>(service.findById(id), HttpStatus.OK);
    }
//    @GetMapping("/all")

    @GetMapping
    public ResponseEntity<List<Author>> getAuthors() {
        return new ResponseEntity<List<Author>>(service.getAll(), HttpStatus.OK);
    }

//    @PostMapping("/author")
    @PostMapping
    public ResponseEntity<String> createAuthor(@RequestBody Author author) {
        service.create(author);
        //Возвращается строка, но можно вернуть и созданный объект.
        return new ResponseEntity<String>("Author created", HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateAuthor(@PathVariable Long id, @RequestBody Author updatableAuthor) {
        if (service.findById(id) == null) {
            //Здесь нужно создать Exception. Пока строка.
            return new ResponseEntity<String>("Account not found", HttpStatus.NOT_FOUND);
        } else {
            service.update(updatableAuthor);//требует задания id в  JSON, иначе ошибка.
            return new ResponseEntity<String>("Account with id=" + id + " was updated", HttpStatus.OK);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAuthor(@PathVariable Long id) {
        if (service.findById(id) == null) {
            //Здесь нужно создать Exception. Пока строка.
            return new ResponseEntity<String>("Account not found", HttpStatus.NOT_FOUND);
        } else {
            service.delete(id);
            return new ResponseEntity<String>("Account with id=" + id + " was deleted", HttpStatus.OK);
        }
    }
}
