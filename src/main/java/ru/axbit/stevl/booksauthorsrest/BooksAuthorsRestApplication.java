package ru.axbit.stevl.booksauthorsrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BooksAuthorsRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooksAuthorsRestApplication.class, args);
	}
}
